def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, num=2)
  ans = "#{word}"
  (num-1).times do
    ans += " #{word}"
  end
  ans
end

def start_of_word(string, num)
  string[0...num]
end

def first_word(string)
  words = string.split
  words[0]
end

def titleize(string)
  small_words = ["and", "over", "the"]
  words = string.split
  ans = "#{words[0].capitalize}"
  words[1..words.length].each do |word|
    if small_words.include?(word)
      ans += " #{word}"
    else
      ans += " #{word.capitalize}"
    end
  end
  ans
end
