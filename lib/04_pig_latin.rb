def translate(string)
  words = string.split
  ans = []
  words.each do |word|
    if is_first_vowel?(word)
      ans << "#{word}ay"
    else
      ans << pl_word(word)
    end
  end
  ans.join(" ")
end


def pl_word(word)
  vowels = ['a', 'e', 'i', 'o']
  i = 0
  while i < word.length
    if vowels.include?(word[i])
      return "#{word[i...word.length]}#{word[0...i]}ay"
    else
      i+=1
    end
  end
end


def is_first_vowel?(word)
  vowels = ['a', 'e', 'i', 'o']
  if vowels.include?(word[0])
    true
  else
    false
  end
end
